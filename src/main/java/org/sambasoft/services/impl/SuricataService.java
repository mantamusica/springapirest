package org.sambasoft.services.impl;

import org.sambasoft.entities.Suricata;
import org.sambasoft.repositories.SuricataRepository;
import org.sambasoft.services.BasicServiceAbstract;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class SuricataService extends BasicServiceAbstract<Suricata, Long> {

    public SuricataService(SuricataRepository suricataRepository) {
        this.repository = suricataRepository;
    }

    public Suricata findSuricataByName(String name) {
        return ((SuricataRepository) repository).findByName(name);
    }

    public Collection<Suricata> findAllByMinimunHeight(Double height) {
        return ((SuricataRepository) repository).findAllByHeightGreaterThanEqual(height);
    }

}